<?php
/**
* Theme functions and definitions
*@link https://developer.wordpress.org/themes/basics/theme-functions/
*
*@package WordPress
*@subpackage smartservice
*@since 1.0.0
*@version 1.0.0
*/

// Scripts & Styles
function smart_styles () {
  wp_register_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '4.1.1' );
  wp_register_style('icons', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array(), '5.2.0');
  wp_register_style('icons', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3.0');
  wp_register_style('style', get_template_directory_uri() . '/style.css', array('bootstrap'), '1.0.1');
  wp_register_style('dafne', get_template_directory_uri() . '/assets/css/dafne.css', array('style'), '1.0.5');
  wp_register_style('fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400', array('dafne'), '1.0.0');
  wp_enqueue_style('bootstrap');
  wp_enqueue_style('icons');
  wp_enqueue_style('style');
  wp_enqueue_style('dafne');
  wp_enqueue_style('fonts');
  wp_enqueue_script('jquery');
  wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'), '4.0.0', true );
  wp_enqueue_script('script', get_template_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'smart_styles');


/* Paginas */
//create the homepage on activation
if (isset($_GET['activated']) && is_admin()){
  $new_page_title = 'Inicio'; //page title
  $new_page_content = ''; //add here the content of the page
  $new_page_template = '';
  $page_check = get_page_by_title($new_page_title);
  $new_page = array(
    'post_type' => 'page',
    'post_title' => $new_page_title,
    'post_content' => $new_page_content,
    'post_status' => 'publish',
    'post_author' => 1,
    'post_slug'     => 'inicio'
  );
  if(!isset($page_check->ID)){
    $new_page_id = wp_insert_post($new_page);
    if(!empty($new_page_template)){
      update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
    }
  }
}

//create the homepage on activation
if (isset($_GET['activated']) && is_admin()){
  $new_page_title = 'Contacto'; //page title
  $new_page_content = ''; //add here the content of the page
  $new_page_template = 'template-contacto.php';
  $page_check = get_page_by_title($new_page_title);
  $new_page = array(
    'post_type' => 'page',
    'post_title' => $new_page_title,
    'post_content' => $new_page_content,
    'post_status' => 'publish',
    'post_author' => 1,
    'post_slug'     => 'contacto'
  );
  if(!isset($page_check->ID)){
    $new_page_id = wp_insert_post($new_page);
    if(!empty($new_page_template)){
      update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
    }
  }
}
//create the catálogo on activation
if (isset($_GET['activated']) && is_admin()){
  $new_page_title = 'Catálogo'; //page title
  $new_page_content = ''; //add here the content of the page
  $new_page_template = 'template-catalogo.php';
  $page_check = get_page_by_title($new_page_title);
  $new_page = array(
    'post_type' => 'page',
    'post_title' => $new_page_title,
    'post_content' => $new_page_content,
    'post_status' => 'publish',
    'post_author' => 1,
    'post_slug'     => 'catalogo'
  );
  if(!isset($page_check->ID)){
    $new_page_id = wp_insert_post($new_page);
    if(!empty($new_page_template)){
      update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
    }
  }
}

//create the galeria on activation
if (isset($_GET['activated']) && is_admin()){
  $new_page_title = 'Galeria'; //page title
  $new_page_content = ''; //add here the content of the page
  $new_page_template = 'template-galeria.php';
  $page_check = get_page_by_title($new_page_title);
  $new_page = array(
    'post_type' => 'page',
    'post_title' => $new_page_title,
    'post_content' => $new_page_content,
    'post_status' => 'publish',
    'post_author' => 1,
    'post_slug'     => 'galeria'
  );
  if(!isset($page_check->ID)){
    $new_page_id = wp_insert_post($new_page);
    if(!empty($new_page_template)){
      update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
    }
  }
}

/* custom fields */
if(function_exists("register_field_group"))
{
  // Catalogo
  register_field_group(array (
		'id' => 'acf_catalogo',
		'title' => 'Catalogo',
		'fields' => array (
			array (
				'key' => 'field_5b75a13b1d5cf',
				'label' => 'Link del embed',
				'name' => 'link_del_embed',
				'type' => 'text',
				'instructions' => 'Agregar el link que contiene el embed dentro del iframe ej: https://www.yumpu.com/es/embed/view/uMGyNgLD1WSh4c',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5b75a1821d5d0',
				'label' => 'Link de descarga',
				'name' => 'link_de_descarga',
				'type' => 'text',
				'instructions' => 'Link para descargar la revista',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-catalogo.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				5 => 'revisions',
				6 => 'author',
				7 => 'format',
				8 => 'featured_image',
				9 => 'categories',
				10 => 'tags',
				11 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}
//sitemap
add_action( 'publish_post', 'itsg_create_sitemap' );
add_action( 'publish_page', 'itsg_create_sitemap' );
function itsg_create_sitemap() {
    $postsForSitemap = get_posts(array(
        'numberposts' => -1,
        'orderby' => 'modified',
        'post_type'  => array( 'post', 'page' ),
        'order'    => 'DESC'
    ));
    $sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
    $sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
    foreach( $postsForSitemap as $post ) {
        setup_postdata( $post );
        $postdate = explode( " ", $post->post_modified );
        $sitemap .= '<url>'.
          '<loc>' . get_permalink( $post->ID ) . '</loc>' .
          '<lastmod>' . $postdate[0] . '</lastmod>' .          
         '</url>';
      }
    $sitemap .= '</urlset>';
    $fp = fopen( ABSPATH . 'sitemap.xml', 'w' );
    fwrite( $fp, $sitemap );
    fclose( $fp );
}
//para los ids unicos
add_filter("gform_field_value_uuid", "get_unique");
function get_unique(){
    $prefix = ""; // update the prefix here
    do {
        $unique = mt_rand();
        $unique = substr($unique, 0, 8);
        $unique = $prefix . $unique;
    } while (!check_unique($unique));
    return $unique;
}
function check_unique($unique) {
    global $wpdb;
    $table = $wpdb->prefix . 'rg_lead_detail';
    $form_id = 1; // update to the form ID your unique id field belongs to
    $field_id = 21; // update to the field ID your unique id is being prepopulated in
    $result = $wpdb->get_var("SELECT value FROM $table WHERE form_id = '$form_id' AND field_number = '$field_id' AND value = '$unique'");
    if(empty($result))
        return true;
    return false;
}


//para hacer los campos de solo lectura
// update '1' to the ID of your form
add_filter( 'gform_pre_render_1', 'add_readonly_script' );
function add_readonly_script( $form ) {
    ?>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            /* apply only to a input with a class of gf_readonly */
            jQuery("li.gf_readonly input").attr("readonly","readonly");
        });
    </script>

    <?php
    return $form;
}

//para dropdown de usuarios

// This adds display names for all users to a drop down box on a gravity form.
add_filter("gform_pre_render", "populate_userdrop");

//Note: when changing drop down values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.
add_filter("gform_admin_pre_render", "populate_userdrop");

function populate_userdrop($form){

    //only populating drop down for form id 1 - if editing this change to your own form ID
    if($form["id"] != 1)

    return $form;

    //Creating item array.
    $items = array();
    // Get the custom field values stored in the array
	// If editing this lookup where you would like to get your data from
	// this example loads through all users of the website
    $metas = get_users();

if (is_array($metas))
{
// in this example we just load the display_name for each user into our drop-down field
foreach($metas as $meta)  $items[] = array("value" => $meta->user_email, "text" => $meta->display_name);
}
		array_unshift($items, array("value"=>"sin_asignar","text"=>"Sin Asignar"));
    //Adding items to field id 1. Replace 1 with your actual field id. You can get the field id by looking at the input name in the markup.
    foreach($form["fields"] as &$field)
        if($field["id"] == 14){
            $field["choices"] = $items;
        }

    return $form;
}

//para ver el rol del usuario
//
add_filter('gform_field_value_user_role', 'gform_populate_user_role');
function gform_populate_user_role($value){
 $user = wp_get_current_user();
 $role = $user->roles;
 return reset($role);
}



//menu
function mis_menus() {
  register_nav_menus(
    array(
      'principal' => __( 'Menu principal' ),
    )
  );
}
add_action( 'init', 'mis_menus' );

//crear clase para <a>
add_filter( 'nav_menu_link_attributes', 'clase_menu', 10, 3 );

function clase_menu($atts, $item, $args){
  $class = 'nav-link t-gris t-white__hover';
  $atts['class'] = $class;
  return $atts;
}
