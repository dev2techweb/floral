<?php
/*
* Template Name: Contacto
*/
get_header(); ?>

<section id="inicio" class="pagina_contacto">
  <div class="container-fluid">
    <h1 class="t-white">CONTACTO</h1>
    <div class="formulario_contacto">
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; endif; ?>
    </div>
  </div>
  
</section>

<?php get_footer()?>











<!-- 

<div class="container-fluid">
  <div class="row a-center" style="background-color: #e50051;background-size:cover; background-position: center;align-items:flex-start;">
    <div class="full-width" style="position:absolute;background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/foto contacto.png');background-size:cover;background-repeat: no-repeat;background-position:top;min-height:115%;">

    </div>
    <div class="col-lg-12 t-center a-center j-center d-flex contacto" style="padding:5rem; padding-top:2rem; flex-direction:column;">
      <div class="formulario_contacto">
        <h2 class="t-pink t-bold">Contacto</h2>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; endif; ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer()?> -->
