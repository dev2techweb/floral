<?php get_header(); ?>
<section id="inicio">
<script>
    $('.carousel').carousel({
   interval: 2000
  })
</script>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/banner.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/banner2.png" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/banner3.png" alt="Third slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/banner4.png" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  <!--<img src="<?php echo get_template_directory_uri(); ?>/assets/img/banner.png" alt="">-->
</section>

<section id="lista1">
<div class="flip-card">
  <div class="flip-card-inner">
    <div class="flip-card-front">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/CATEGORIAS.png" alt="Avatar">
    </div>
    <div class="flip-card-back">
    <hr/>
      <h1>Arreglos de Amor</h1>
      <p>Click para ver mas</p>
      <a href="https://stilofloral.com/category/amor/"><h2>Arreglos de Amor</h2></a>
    </div>
  </div>
</div>
<div class="hr-vertical" style=""></div>
<div class="flip-card">
  <div class="flip-card-inner">
    <div class="flip-card-front">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/CATEGORIAS3.png" alt="Avatar" >
    </div>
    <div class="flip-card-back">
    <hr/>
      <h1>ArreglosCumpleaños</h1>
      <p>Click para ver mas</p>
      <a href="https://stilofloral.com/category/cumpleanos/"><h2>Arreglos de Amor</h2></a>
    </div>
  </div>
</div>
<div class="hr-vertical" style=""></div>
<div class="flip-card">
  <div class="flip-card-inner">
    <div class="flip-card-front">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/CATEGORIAS4.png" alt="Avatar" >
    </div>
    <div class="flip-card-back">
    <hr/>
      <h1>Arreglos Funebres</h1>
      <p>Click para ver mas</p>
      <a href="https://stilofloral.com/category/funebre/"><h2>Arreglos de Amor</h2></a>
    </div>
  </div>
</div>

<div class="hr-vertical" style=""></div>


<div class="flip-card">
  <div class="flip-card-inner">
    <div class="flip-card-front">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/CATEGORIAS2.png" alt="Avatar" >
    </div>
    <div class="flip-card-back">
    <hr/>
      <h1>Rosas Preservadas</h1>
      <p>Click para ver mas</p>
      <a href="https://stilofloral.com/category/rosas-preservadas/"><h2>Arreglos de Amor</h2></a>
    </div>
  </div>
</div>
</section>
<?php echo do_shortcode( '[recent_products per_page="32" paginate="true"]' ); ?>
<!--
<section id="lista2">
  <div class="columna" id="c1">
    <div class="elemento">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arreglo.png" alt="">
    </div>
    <div class="elemento">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arreglo.png" alt="">
    </div>
  </div>
  <div class="columna" id="c2">
    <div class="elemento">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arreglo.png" alt="">
    </div>
    <div class="elemento">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arreglo.png" alt="">
    </div>
  </div>
</section>
-->
<section id="bancos">
  
</section>
<section id="misionvision">
</section>
<section id="bancosimagen">
  
</section>
<?php get_footer()?>
