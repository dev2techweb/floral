<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title('-',true,'right'); ?><?php bloginfo('name');?></title>
    <?php wp_head(); ?>
    
</head>

<body>
    <header class="header b-transparent full-width">
        <a href="<?php echo esc_url(home_url('/'));?>">
            <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="">
        </a>
        <div class="container-fluid a-center j-center d-flex" style="flex-direction:column;">
            <div class="row a-right clean">
                <nav class="navbar-expand-lg header__nav col-lg-12" style="">
                    <button class="navbar-toggler e-center" type="button" data-toggle="collapse"
                        data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="fa fa-bars t-pink"></span>
                    </button>
                    <?php wp_nav_menu(array(
              'theme_location' => 'principal',
              'container' => 'div',
              'container_class' => 'collapse navbar-collapse j-center',
              'container_id' => 'navbarNav',
              'items_wrap' => '<ul class="navbar-nav">%3$s</ul>',
              //clase del elemento li
              'menu_class' => 'nav-item'
            )); ?>
                </nav>
            </div>
        </div>
        <div class="nav-contacto">
            <h4>Llámanos:</h4><br>
                <h4>
                3230-2821</h4><br>
        </div>
        <div class="nav-social">
        <a href="https://www.instagram.com/stilo.floral/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/instagram.png" alt="">
            </a>
            <a href="https://www.facebook.com/stilofloralgt/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.png" alt="">
            </a>
        </div>
    </header>
    <main class="main">